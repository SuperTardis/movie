import {Films} from "./database"

export const getFilms = (req, res, next) => {
	Films.find((err, films)=> {
			if (!err)
				return res.send({status: 'OK', films: films});
			else {
				res.statusCode = 500;
				return res.send({error: 'Nothing'});
			}
		});
	next();
};

export const deleteFilm = (req, res, next) => {
	Films.findByIdAndRemove({_id : req.params.id}, (error) =>{
		if (!error)
			res.send({status: 'OK'});
		});
	next();
};

export const addFilms = (req, res, next) => {
	req.body.format = {kind: req.body.format};

	let film = new Films(req.body);
	console.log(film.title);
	film.save((err) => {
		if (err) {
			res.send({
				status: 'error', error: err
			});
		} else
			res.send({status: 'OK'});
	});
	next();
};