const validate = values => {
	const errors = {};

	if (!values.title || values.title.trim().length === 0) {
		errors.title = 'Required';
	}
	if (!values.release) {
		errors.release = 'Required';
	}
	else if (parseInt(values.release) < 1888)
		errors.release = 'Must be more then 1888';
	else if (values.release.length > 4)
		errors.release = "Year must have 4 digits";
	if (!values.format) {
		errors.format = 'Required';
	}
	if (!values.stars || !values.stars.length) {
		errors.stars = { _error: 'At least one stars must be entered' };
	} else {
		const starsArrayErrors = [];
		values.stars.forEach((star, starsIndex) => {
			const starsErrors = {};
			if (!star || !star.firstName || star.firstName.trim().length === 0) {
				starsErrors.firstName = 'Required';
				starsArrayErrors[starsIndex] = starsErrors;
			}
			if (!star || !star.lastName || star.lastName.trim().length === 0) {
				starsErrors.lastName = 'Required';
				starsArrayErrors[starsIndex] = starsErrors;
			}
		});
		if (starsArrayErrors.length) {
			errors.stars = starsArrayErrors;
		}
	}
	return errors;
};

export default validate;
