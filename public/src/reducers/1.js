
import { ADD_TODO, GET_FILM, GET_FILM_SUCCESS } from '../actions/actions'



function film(state = initialState, action) {
	switch (action.type) {
		case GET_FILM_SUCCESS:
			return {...state, photos: action.payload};
		default:
			return state
	}
}

function todo(state, action) {
	switch (action.type) {

		case ADD_TODO:
			return {
				id: action.id,
				text: action.text,
			};

		default:
			return state
	}
}

function todos(state = [], action) {
	switch (action.type) {

		case ADD_TODO:
			return [
				...state,
				todo(undefined, action)
			];

		default:
			return state;
	}
}

// const rootReducer = combineReducers({
// 	todos
// });

// export default rootReducer