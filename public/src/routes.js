import React from 'react'
import { Link, Route, Switch, withRouter } from 'react-router-dom'
import Filter from './components/Home.jsx'
import AddFilm from './components/AddFilm.jsx'
import { connect} from 'react-redux'

class Routes extends React.Component {
	render() {
		const { page } = this.props;
		return (
			<div id="main">
				<div id="sidebar">
					<div id="logo"><Link to="/"><img src="/public/styles/imgs/movie_logo.png" alt=""/></Link></div>
					<div id="menu" className="menu-v">
						<header>
							<nav>
						<ul>
							<li><Link to="/" className="active">Movie List</Link>
							</li>
							<li><Link to="/add_film" className="active">Add Film</Link>
							</li>
						</ul>
							</nav>
						</header>
					</div>
				</div>
				<div className="content">
					<Main/>
					{this.props.children}
				</div>
			</div>
		)
	}
}

class Main extends React.Component {
	render() {
		return (
			<main>
			<Switch>
				<Route exact path="/" component={Filter}/>
				<Route path="/add_film" component={AddFilm}/>
			</Switch>
			</main>);
	}
}
function mapStateToProps (state) {
	return {
		page: state.page
	}
}

export default withRouter(
	connect(mapStateToProps)(Routes)
);