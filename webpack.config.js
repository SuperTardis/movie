var config = {
	entry: './public/src/index.js',

	output: {
		path: __dirname + '/',
		filename: 'public/bundle.js'
	},

	devServer: {
		inline: true,
		port: 8082
	},

	module: {
		loaders: [
			{
				test: /\.(jsx?)$/,
				exclude: /node_modules/,
				loader: 'babel-loader',

				query: {
					presets: ['es2015', 'react', 'stage-2']
				}
			},
			{
				test: /\.css$/,
				loader: "style-loader!css-loader"
			},
			{
				test: /\.(jpe?g|png|gif|svg)$/i,
				use: [
					'url-loader?limit=10000',
					'img-loader'
				]
			}
		]
	}
};

module.exports = config;