# README #

* This project is designed to search, add and delete movies.
* Frameworks, tolls and web modules used: ES6, NodeJS + ExpressJS, ReactJS, Redux architecture, Webpack, npm, yarn, MongoDB

### Setup ###

* Download and install if you have not NodeJS https://nodejs.org
* Download and install if you have not MongoDB https://www.mongodb.com/
* Clone this repo: 
```
  git clone "repo link"
```
* Install yarn using next comand: 
```
npm install yarn -g
```
* Pass to cloned folder: 
```
  cd "cloned folder"
```
* Configure config.json file with following:
```
{
  "port" : 8089, 
  "mongoose":{
    "uri": "mongodb://127.0.0.1/filmdb"
  }
}
```
* port - is backend Server port
* uri - is addres MongoDB
* Use next comands
```
yarn install
yarn server_start
yarn start
```

### Architecture ###
![Architecture](images/image.png)
### Author ###

* Porechna Polina(polinaporechna@gmail.com)